--   test-languages
--   Copyright (C) 2017  Royer Cédric
--
--   This program is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   This program is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with this program.  If not, see <http://www.gnu.org/licenses/>.

data Result = Less | More | Equal deriving (Show, Eq)

guess :: Integer -> Integer -> Result
guess expected given | given < expected  = Less
                     | given == expected = Equal
                     | given > expected  = More

infinite_guess :: Integer -> IO ()

infinite_guess expected = do putStrLn "Enter a number: "
                             raw_input <- getLine
                             let input = read raw_input :: Integer
                             let res = guess expected input
                             if res == Equal
                             then putStrLn "You win !!!"
                             else putStrLn ("You give a number that is " ++ show res) >>
                                 infinite_guess expected
                            

main = infinite_guess 100
