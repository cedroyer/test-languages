type Float = f64;

#[derive(Clone, Copy)]
pub struct Point {
    x: Float,
    y: Float,
}

fn dist_2(a: Point, b: Point) -> Float {
    ((a.x - b.x).powi(2) + (a.y - b.y).powi(2)).sqrt()
}

pub struct CircleEntity {
    pos: Point,
    radius: Float,
}

impl CircleEntity {
    fn collide(&self, other: &CircleEntity) -> bool {
        dist_2(self.pos, other.pos) < self.radius + other.radius
    }
}

pub struct RectangleBoard {
    x_min: Float,
    x_max: Float,
    y_min: Float,
    y_max: Float,
}

impl RectangleBoard {
    fn new(width: Float, height: Float) -> RectangleBoard {
        RectangleBoard {
            x_min: -width / 2.0,
            x_max: width / 2.0,
            y_min: -height / 2.0,
            y_max: height / 2.0,
        }
    }
    fn is_in(&self, e: CircleEntity) -> bool {
        e.pos.x - e.radius >= self.x_min
            && e.pos.x + e.radius <= self.x_max
            && e.pos.y - e.radius >= self.y_min
            && e.pos.y + e.radius <= self.y_max
    }
}

pub struct CircleBoard {
    radius: Float,
}

impl CircleBoard {
    fn is_in(&self, e: CircleEntity) -> bool {
        dist_2(Point { x: 0.0, y: 0.0 }, e.pos) + e.radius <= self.radius
    }
}

pub struct AllEntities {
    entities: Vec<CircleEntity>,
}

pub struct Collision<'a> {
    first: &'a CircleEntity,
    second: &'a CircleEntity,
    contact_pos: Point,
}

pub struct CollisionIterator<'a> {
    all_entities: &'a AllEntities,
    current_entity: usize,
    sub_current_entity: usize,
}

impl<'a> Iterator for CollisionIterator<'a> {
    type Item = Collision<'a>;
    fn next(&mut self) -> Option<Collision<'a>> {
        for entity in self.all_entities.entities.get(self.current_entity..)? {
            self.current_entity += 1;
            for other in self
                .all_entities
                .entities
                .get(self.sub_current_entity..)
                .unwrap_or(&[])
            {
                self.sub_current_entity += 1;
                if entity.collide(other) {
                    return Some(Collision::<'a> {
                        first: entity,
                        second: other,
                        contact_pos: entity.pos,
                    });
                }
            }
            self.sub_current_entity = self.current_entity + 1;
        }
        None
    }
}

impl AllEntities {
    fn iter_collision(&self) -> CollisionIterator {
        CollisionIterator {
            all_entities: &self,
            current_entity: 0,
            sub_current_entity: 1,
        }
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use std::iter::FromIterator;

    #[test]
    fn test_dist_2() {
        let a = Point { x: 1.0, y: 2.0 };
        let b = Point { x: 1.0, y: 4.0 };
        assert_eq!(dist_2(a, b), 2.0);
    }

    #[test]
    fn test_rectangle_board_is_in() {
        let b = RectangleBoard::new(20.0, 40.0);
        assert!(b.is_in(CircleEntity {
            pos: Point { x: 1.0, y: 15.0 },
            radius: 0.1
        }));
        assert!(!b.is_in(CircleEntity {
            pos: Point { x: 20.0, y: 2.0 },
            radius: 0.1
        }));
    }

    #[test]
    fn test_circle_board_is_in() {
        let b = CircleBoard { radius: 2.0 };
        assert!(b.is_in(CircleEntity {
            pos: Point { x: 1.0, y: 1.0 },
            radius: 0.1,
        }));
        assert!(!b.is_in(CircleEntity {
            pos: Point { x: 2.0, y: 2.0 },
            radius: 0.1,
        }));
    }

    #[test]
    fn test_circle_entity_collide() {
        let a = CircleEntity {
            pos: Point { x: 1.0, y: 2.0 },
            radius: 0.5,
        };
        let b = CircleEntity {
            pos: Point { x: 1.0, y: 4.0 },
            radius: 0.5,
        };
        let c = CircleEntity {
            pos: Point { x: 1.2, y: 4.1 },
            radius: 0.5,
        };
        assert!(!a.collide(&b));
        assert!(b.collide(&c));
    }

    #[test]
    fn test_iter_collision() {
        let a = CircleEntity {
            pos: Point { x: 1.0, y: 2.0 },
            radius: 0.5,
        };
        let b = CircleEntity {
            pos: Point { x: 1.0, y: 4.0 },
            radius: 0.5,
        };
        let c = CircleEntity {
            pos: Point { x: 1.2, y: 4.1 },
            radius: 0.5,
        };
        let d = CircleEntity {
            pos: Point { x: 1.1, y: 4.05 },
            radius: 2.0,
        };
        let all_entities = AllEntities {
            entities: vec![a, b, c, d],
        };
        let collisions = Vec::<Collision>::from_iter(all_entities.iter_collision());
        assert_eq!(collisions.len(), 2)
    }

    #[test]
    fn test_iter_collision_empty() {
        let all_entities = AllEntities { entities: vec![] };
        let collisions = Vec::<Collision>::from_iter(all_entities.iter_collision());
        assert_eq!(collisions.len(), 0)
    }
}
