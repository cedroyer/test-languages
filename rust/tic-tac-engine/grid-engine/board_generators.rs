use crate::models::Board;
use crate::models::CellKind;

fn generate_simple_board(width: usize, height: usize, kind: CellKind) -> Board {
    let mut board = Board::with_capacity(height);
    for _row in 0..height {
        board.push(vec![kind; width]);
    }
    board
}

#[cfg(tests)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_simple_board() {
        let board = generate_simple_board(100, 150, CellKind::Forest);

        assert_eq!(board[5][5], CellKind::Forest);
    }
}
