use super::board::Pos;

struct BluePrint {
    name: String,
}

struct Unit {
    blue_print: BluePrint,
    pos: Pos,
    hp: f32,
    sp: f32,
}

impl Unit {
    fn is_dead(&self) -> bool {
        self.hp <= 0.0
    }
}
