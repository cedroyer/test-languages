#[derive(Clone, Copy)]
pub enum CellKind {
    Road,
    Land,
    Forest,
    Mountain,
    Building(i8),
}

pub type Board = Vec<Vec<CellKind>>;

pub struct Pos(usize, usize);
